package com.muneeba.internetdetectorbackgroundservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

public class CheckConnectivity extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent arg1) {

        if (!InternetDetector.detectInternet(context)) {
            PrintString(context, "Internet Connection Lost");
        } else {
            PrintString(context, "Internet Connection");
        }
    }


    // our native method, which will be called from Unity3D
    public void PrintString(final Context ctx, final String message) {
        //create / show an android toast, with message and short duration.
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}